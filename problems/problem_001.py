# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# 1
# inputs put in two numerical values that can be the same
# outputs the smaller of the two numerical values

# 2
# minimum_value_one = (27,19)
# minimum_value_two = (18,18)
# minimum_value_three = (16,10)

# 3

# def minimum_value(value1, value2):
#     pass

# 4
# result1 = minimum_value(minimum_value_one)
# result2 = minimum_value(minimum_value_two)
# result3 = minimum_value(minimum_value_three)
# print(result1)
# print(result2)
# print(result3)

# 5 I'll need to compare both values to see which one is larger or smaller.
# if they are the same i can use either solution

# 6
# Looks like i can use a series of "if" statements to see whether or not
# value1 is greater than or equal to value2

# 7
def minimum_value(value1, value2):
    #if statement to see if value1 is equal to value2 return value1 if they are the same
    if value1 == value2:
        return value1
    #elif statement to see if value1 is greater than value2 return value2 since its smaller
    elif value1 > value2:
        return value2
    #elif statement if value2 is greater than value1 return value1
    elif value2 > value1:
        return value1

minimum_value_one = minimum_value(27, 19)
minimum_value_two = minimum_value(18, 18)
minimum_value_three = minimum_value(16, 10)

# result1 = minimum_value_one
# result2 = minimum_value_two
# result3 = minimum_value_three
print(minimum_value_one)
print(minimum_value_two)
print(minimum_value_three)
