# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#input 3 numerical values
#output is the value with large value

def max_of_three(value1, value2, value3):
    # if statement comparing the three values to see if value1 is greater than value2 an greater than value3
    if value1 >= value2 and value1 >= value3:
        return value1
    # elif statement to see if value2 is greater than value1 and greater than value3
    elif value2 >= value1 and value2 >= value3:
        return value2
    #elif statement to see if value 3 is greater than value1 and greater than value2
    elif value3 >= value1 and value3 >= value2:
        return value3



max_test1 = max_of_three(13, 14, 15)
max_test2 = max_of_three(1, 5, 7)
max_test3 = max_of_three(-1, 4, 10.0)
print(max_test1)
print(max_test2)
print(max_test3)
