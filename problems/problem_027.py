# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

#Inputs a list of numerical Values
#Outputs the largest value in the list


def max_in_list(values):
    # Edge case if the list is empty then return none
    if len(values) == 0:
        return None
    #created a variable that returns the largest value in a numerical list
    max_value = max(values)
    return max_value



test1 = max_in_list([1, 2, 3, 500, 879, 1000])
test2 = max_in_list([])

print(test1)
print(test2)
