# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#inputs any word
#outputs if its truely a palindrome word or not



def is_palindrome(word):
    # converts word into a list and compares if the lists are the same even if its spelled backwards
    if list(word) == list(reversed(word)):
        return True
    else:
        return False

result1 = is_palindrome("civic")
result2 = is_palindrome("airplane")
print(result1)
print(result2)
