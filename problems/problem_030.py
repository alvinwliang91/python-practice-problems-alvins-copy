# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#Inputs is a list of numerical values
#Outputs the second largest number in a list
#

def find_second_largest(values):
    if len(values) <= 1:
        return None
    new_list = set(values)
    new_list.remove(max(new_list))
    return max(new_list)


test1 = [1,2,3,4,5]
test2 = []


print(find_second_largest(test1))
print(find_second_largest(test2))
