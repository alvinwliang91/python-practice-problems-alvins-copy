# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average


#Inputs is a list of numerical values between 0 and 100
# Outputs A letter grade based on the average of the list of numbers

def calculate_grade(values):
    # Edge case when there are no values in the list.
    if len(values) == 0:
        return None
    # create a variable for calculation of averaged grades. Sum of all values in the list
    # divded by length of the list
    averaged_grades = sum(values) / len(values)
    # If statements to see what grade the average of the list will get. A B C D or an F
    if averaged_grades >= 90:
        return "A"
    elif averaged_grades >= 80:
        return "B"
    elif averaged_grades >= 70:
        return "C"
    elif averaged_grades >= 60:
        return "D"
    else:
        return "F"



test1 = calculate_grade([47, 76, 89, 100, 67])
test2 = calculate_grade([78, 99, 97, 100, 100, 95])
test3 = calculate_grade([49])
test4 = calculate_grade([80])
test5 = calculate_grade([65])
test6 = calculate_grade([])

print(test1)
print(test2)
print(test3)
print(test4)
print(test5)
print(test6)
