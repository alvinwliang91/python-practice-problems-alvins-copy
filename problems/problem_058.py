# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(param1):
    #create an empty dictionary to append into
    my_dict = {}
    #for loop to iterate over lists
    for city in param1:
        # split the cities from the states by ,
        name, state = city.split(",")
        # strip the states
        state = state.strip()
        # if state is not in dictionary
        if state not in my_dict:
            # if its true then it'll add state into dictionary
            my_dict[state] = []
            #Adds in the city name that the state was associated with
        my_dict[state].append(name)
    return my_dict







list1 = ["Hayward, CA"]
list2 = ["Springfield, MA", "Boston, MA"]
list3 = ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
print(group_cities_by_state(list1))
print(group_cities_by_state(list2))
print(group_cities_by_state(list3))
