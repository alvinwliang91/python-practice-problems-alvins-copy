# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

# inputs numerical values
# outputs returns in bounds otherwise out of bounds

def is_inside_bounds(x, y):
    # if statement to see if x,y is between 0 and 10
    if x >= 0 and x <= 10 and y >= 0 and y <= 10:
        return True
    else:
        return False

test1 = is_inside_bounds(1,5)
test2 = is_inside_bounds(12,15)
print(test1)
print(test2)
