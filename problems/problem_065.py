# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(param1):
    # determine initial gap between index 1 and 0
    max_gap = abs(param1[1] - param1[0])
    #for loop with between 1 and last number
    for i in range(1, len(param1)-1):
        # Index + 1 (next index) - current index
        gap = abs(param1[i + 1] - param1[i])
        # if the gap is greater than the max_garp earlier then set the max_gap to new value
        if gap > max_gap:
            max_gap = gap
    return max_gap


param1 = [1,4,5,6,17]
print(biggest_gap(param1))
