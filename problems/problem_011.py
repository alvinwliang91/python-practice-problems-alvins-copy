# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# inputs numerical value
# outputs buzz if the number is divisible by 5 otherwise just returns the number

def is_divisible_by_5(number):
    #if statement to see if number is divisible by 5
    if number % 5 == 0:
        return "buzz"
    else:
        return number

test1 = is_divisible_by_5(10)
test2 = is_divisible_by_5(11)
print(test1)
print(test2)
