# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

#inputs a list parameter and a search item parameter to find a specific item in the list parameter
#outputs new list that contains the index of the searched for item


def find_indexes(search_list, search_term):
    # create a new variable to add new list to
    new_list = []
    # for loop of the search list to iterate over
    for i, element in enumerate(search_list):
        # if index matches the search term print I the index
        if element == search_term:
            #if does match add back the index at element i to new_list
            new_list.append(i)
    return new_list




param1 = [1, 2, 3, 4]
param2 = 4
print(find_indexes(param1, param2))
