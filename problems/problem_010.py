# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# inputs a numerical number
# outputs if the number is divisible by 3 without a remainder return the word "fizz".
# otherwise return word number

def is_divisible_by_3(number):
    # if statement to see if "number" can be divisbile by 3 if so return the word "fizz"
    if number % 3 == 0:
        return "fizz"
    else:
        return number


test1 = is_divisible_by_3(10)
test2 = is_divisible_by_3(9)
print(test1)
print(test2)
