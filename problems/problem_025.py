# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

# inputs list of numerical values
# outputs returns the sum of numbers in the list


def calculate_sum(values):
    #if list of values has no values return None
    if len(values) == 0:
        return None
    total = sum(values)
    return total



test1 = calculate_sum([1,2,3,4,5])
test2 = calculate_sum([])
print(test1)
print(test2)
