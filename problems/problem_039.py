# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

#Inputs a dictionary
#Outputs a dictionary with the key and values reversed

# def reverse_dictionary(dictionary):
#     my_dict = {}
#     for key, value in dictionary.items():
#         my_dict[value] = key
#         print(key)
#         print(value)

#     for key, value in my_dict.items():
#         print(key)
#         print(value)

#     print(my_dict)

# worker = {"name": "Alvin", "age": 32, "job": "Janitor"}
# reverse_dictionary(worker)

def reverse_dictionary(dictionary):
    my_dict = {}
    for key, value in dictionary.items():
        my_dict[value] = key
    return my_dict




worker = {"name": "Alvin", "age": 32, "job": "Janitor"}
print(reverse_dictionary(worker))
