# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#Inputs a numerical list
#outputs the sum of each item squared

def sum_of_squares(values):
    if len(values) == 0:
        return None
    # create a new list for squared items to go into
    squared = []
    # for loop to square each item in the list
    for numbers in values:
        squared.append(numbers** 2)
    return sum(squared)



test1 = [1,2,3,4,5]
test2 = [1,2,3]

print(sum_of_squares(test1))
print(sum_of_squares(test2))
