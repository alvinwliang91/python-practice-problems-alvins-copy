# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#inputs list of ingredients
# outputs if ingredients are in the list return true otherwise return false

def can_make_pasta(ingredients):
    # if statement to see if each ingredient are in ingredients
    if "flour" in ingredients and "eggs" in ingredients and "oil" in ingredients:
        return True
    else:
        return False


test1 = can_make_pasta(("flour", "eggs", "oil"))
test2 = can_make_pasta(("oil", "eggs", "flour"))
print(test1)
print(test2)
