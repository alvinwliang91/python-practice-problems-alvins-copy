# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# inputs age and if they've signed a consent
# outputs true or false if they can go sky diving


def can_skydive(age, has_consent_form):
    #if they have a consent form then they can skydive or they are older than 18
    if age > 17 or has_consent_form == True:
        return print("You can skydive!")
    else:
        return print("You can't skydive, sorry!")

result1 = can_skydive(18, False)
result2 = can_skydive(17, True)
result3 = can_skydive(17, False)
