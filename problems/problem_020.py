# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

# inputs numerical value
# outputs if first list has 50% more people than second list return true otherwise return false

def has_quorum(attendees_list, members_list):
    if (attendees_list * .5) >= members_list:
        return True
    else:
        return False


test1 = has_quorum(50, 25)
test2 = has_quorum(50, 26)
# test3 = has_quorum()
print(test1)
print(test2)
