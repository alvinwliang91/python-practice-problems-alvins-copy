# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# inputs number, number for characters in result, character to add in
# outputs a string with the first number on the right with number of characters added to left

def pad_left(number, length, pad):
    result = str(number)
    for i in range(length):
        if len(result) < 4:
            result = pad + result
    return result


test1 = pad_left(10, 4, "*")
print(test1)
## result "**10"
