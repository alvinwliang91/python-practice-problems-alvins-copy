# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# I think I should use pop to remove duplicates and append to add to new list
# should create a variable for a new empty list to be added to.
# input a string variable
# output a list with all the duplicates gone

def remove_duplicate_letters(s):
    result = ""
    for i in s:
        if i not in result:
            result = result + i
    return result


test1 = remove_duplicate_letters("hello")
print(test1)
