# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem


def shift_letters(param1):
    empty_string = ""
    for letter in param1:
        if letter == "Z":
            new_letter = "A"
        elif letter == "z":
            new_letter = "a"
        else:
            new_letter = chr(ord(letter) + 1)
        empty_string += new_letter
    return empty_string

param1 = "abba"
param2 = "zap"
print(shift_letters(param1))
# "bccb"
print(shift_letters(param2))
# 'abq'


# print(ord("a"))
