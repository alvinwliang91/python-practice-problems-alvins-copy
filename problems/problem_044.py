# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

#Inputs list of keys and a dictionary
#Outputs a list with all of the values and a dictionary with the key paid together with the values

def translate(key_list, dictionary):
    # New list to add values in
    values_list = []
    #for loop to iterate over the keys list
    for key in key_list:
        values_list.append(dictionary.get(key))
    return values_list



param1 = ["name","age", "height"]
param2 = {"name": "Alvin", "age": "32", "height": "5'11"}
print(translate(param1,param2))
