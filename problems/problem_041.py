# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

#Inputs a list
#outputs a list where the csv values are added together


def add_csv_lines(csv_lines):
    # create a variable with empty results list to be added into
    result_list = []
    #for loop to go over split the items in the lists
    for item in csv_lines:
        #create a new variable pieces to hold item split
        pieces = item.split(",")
        # a new variable to hold sum of the items split
        line_sum = 0
        # inner for loop to iterate over items inside the list
        for piece in pieces:
            # make sure returned value is an integer
            value = int(piece)
            # line_sum will add all pieces together
            line_sum = line_sum + value
        # After calculation adds back the new results list
        result_list.append(line_sum)
    return result_list


test1 = ["1,2,3", "1,7"]
print(add_csv_lines(test1))
# # [6, 8]



# string = "1,2,3,4"
# words = string.split(',')
# print(words)


# words = '1,2,3'.split(',')
# print(words)
