# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# inputs true of false. if its a work day or if its sunny
# outputs list of gear needed for that type of day

def gear_for_day(is_workday, is_sunny):
    packing_list=[]
    if is_workday and not is_sunny:
        packing_list.append("umbrella")
    if is_workday:
       packing_list.append("laptop")
    else:
        packing_list.append("surfboard")
    return packing_list



test1 = gear_for_day(True,False)
test2 = gear_for_day(True, True)
test3 = gear_for_day(False, False)
print(test1)
print(test2)
print(test3)
