# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# inputs numerical values
# outputs if both outputs are divisible by 3 and 5 return "fizzbuzz"
# if only divisible by 3 then return "fizz"
# if only divisible by 5 then return "buzz"
# else return number

def fizzbuzz(number):
    #if statement to see if number divisible by 3 and 5
    if number % 3 == 0 and number % 5 ==0:
        return "fizzbuzz"
    #elif if number is divisible by 3
    elif number % 3 == 0:
        return "fizz"
    #elif if number is divisible by 5
    elif number % 5 == 0:
        return "buzz"
    else:
        return number


test1 = fizzbuzz(15)
test2 = fizzbuzz(2)
test3 = fizzbuzz(3)
test4 = fizzbuzz(5)
print(test1)
print(test2)
print(test3)
print(test4)
