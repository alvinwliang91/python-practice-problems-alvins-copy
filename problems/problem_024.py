# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# inputs Numerical list of the numbers
# outputs Average of added numbers


def calculate_average(values):
   # if there is no values in list then return none
   if len(values) == 0:
    return None
    #create a variable to hold the average and calcuate sum of list divided but number of numbers in the list
   else:
    average_total = sum(values) / len(values)

    return int(average_total)


test1 = calculate_average([1,2,3,4,5])
test2 = calculate_average([10,20,30,40,50])
test3 = calculate_average([])
print(test1)
print(test2)
print(test3)
