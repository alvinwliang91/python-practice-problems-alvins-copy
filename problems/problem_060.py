# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

def only_odds(param1):
    odds_only = []
    for odds in param1:
        if odds % 2 ==1:
            odds_only.append(odds)
    return odds_only



param1 = [1,2,3,4,5]
param2 = [1,3,48,97,23]
print(only_odds(param1))
print(only_odds(param2))
