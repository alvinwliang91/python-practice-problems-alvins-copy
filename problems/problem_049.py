# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(x, y):
    added = x + y
    return added

param1 = 1
param2 = 3
print(sum_two_numbers(param1, param2))
